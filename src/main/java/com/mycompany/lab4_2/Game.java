/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4_2;

import java.util.Scanner;

/**
 *
 * @author User
 */
public class Game {
    private Player player1, player2;
    private Board board;
    
    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }
    public void play() {
        boolean isFinish=false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTurn();
            inputRowCol();
            if (board.checkWin()) {
                printBoard();
                printWinner();
                isFinish=true;
            }
            if (board.checkDraw()) {
                printBoard();
                printDraw();
                isFinish=true;
            }
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to OX Game");
    }

    private void printBoard() {
        char[][] b = board.getBoard();
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++) {
                System.out.print(b[i][j]+"");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println(board.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row col:");
        int row = sc.nextInt();
        int col = sc.nextInt();
        board.setRowCol(row, col);
    }

    private void newGame() {
        board = new Board(player1, player2);
    }

    private void printWinner() {
        System.out.println(board.getCurrentPlayer() + "Win!!");
    }

    private void printDraw() {
        System.out.println("Draw!");
    }
}
