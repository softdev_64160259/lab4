/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4_2;

/**
 *
 * @author User
 */
public class Player {
    private char symbol;
    private int winCount, loseCount, drawCount;
    
    public Player(char symbol) { 
        this.symbol = symbol;
    }
    
    public char getSymbol() {
        return symbol;
    }

    
}
